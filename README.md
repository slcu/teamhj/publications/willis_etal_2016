# Introduction

This repository includes and refer to scripts used to produce the results in the paper

Willis L, Refahi Y, Wightman R, Landrein B, Teles J, Huang KC, Meyerowitz EM, Jönsson H (2016)
Cell size and growth regulation in the Arabidopsis thaliana apical stem cell niche
Proc Natl Acad Sci USA 113(51) E8238-E8246
https://doi.org/10.1073/pnas.1616768113

The more detailed information on the methods used are provided in the 
[Supporting Information](https://www.pnas.org/content/pnas/suppl/2016/12/02/1616768113.DCSupplemental/pnas.1616768113.sapp.pdf) 
for the paper.

# Data

Confocal stacks of the time series data including preprocessing (z-scaling) information, 
segmented stacks, and tracking data are provided in the repository

[Research data supporting 'Cell size and growth regulation in the Arabidopsis thaliana apical stem cell niche'](https://doi.org/10.17863/CAM.7793)

# Scripts and software

The scripts have been divided into two folders

<p><i>segmentation_tracking</i></p>
<p><i>analysis</i></p>

The first folder holds information on how the segmentation and tracking was performed, 
the second holds instructions and scripts for extracting data from the segmented data.

# Contact

lisa.willis@slcu.cam.ac.uk and 
yassin.refahi@slcu.cam.ac.uk and 
henrik.jonsson@slcu.cam.ac.uk 



allRootPaths, allSavePaths, allTimesList, allALTDataPathList, allSegDataPathList = [], [], [], [], []
allMeanVolumes, allL1centre, allIndicesTEndBack, allIndicesTEndForward, allIndicesTBegin, allIndicesL1Init, allPlants = [], [], [], [], [], [], [] 

"""
allPlants.append("plant 13")
from config_2013_10_07_plant13 import rootPath, savePath, timesList, ALTDataPathList, segDataPathList
allRootPaths.append(rootPath), allSavePaths.append(savePath), allTimesList.append(timesList), allALTDataPathList.append(ALTDataPathList), allSegDataPathList.append(segDataPathList)
from indices_parameters_2013_10_07_plant13 import meanVolumes, L1centre, indicesTEndBack, indicesTEndForward, indicesTBegin
allMeanVolumes.append(meanVolumes), allL1centre.append(L1centre), allIndicesTEndBack.append(indicesTEndBack), allIndicesTEndForward.append(indicesTEndForward)
allIndicesTBegin.append(indicesTBegin)

allPlants.append("primordia 1")
from config_Yassin_primordia1 import rootPath, savePath, timesList, ALTDataPathList, segDataPathList
allRootPaths.append(rootPath), allSavePaths.append(savePath), allTimesList.append(timesList), allALTDataPathList.append(ALTDataPathList), allSegDataPathList.append(segDataPathList)
from indices_parameters_Yassin_primordia1 import meanVolumes, L1centre, indicesTEndBack, indicesTEndForward
allMeanVolumes.append(meanVolumes), allL1centre.append(L1centre), allIndicesTEndBack.append(indicesTEndBack), allIndicesTEndForward.append(indicesTEndForward)
allIndicesTBegin.append(indicesTBegin)

allPlants.append("primordia 2")
from config_Yassin_primordium2 import rootPath, savePath, timesList, ALTDataPathList, segDataPathList
allRootPaths.append(rootPath), allSavePaths.append(savePath), allTimesList.append(timesList), allALTDataPathList.append(ALTDataPathList), allSegDataPathList.append(segDataPathList)
from indices_parameters_Yassin_primordium2 import meanVolumes, L1centre, indicesTEndBack, indicesTEndForward
allMeanVolumes.append(meanVolumes), allL1centre.append(L1centre), allIndicesTEndBack.append(indicesTEndBack), allIndicesTEndForward.append(indicesTEndForward)
allIndicesTBegin.append(indicesTBegin)

"""
allPlants.append("plant 1")
from config_2015_02_16_plant1 import rootPath, savePath, timesList, ALTDataPathList, segDataPathList
allRootPaths.append(rootPath), allSavePaths.append(savePath), allTimesList.append(timesList), allALTDataPathList.append(ALTDataPathList), allSegDataPathList.append(segDataPathList)
from indices_parameters_2015_02_16_plant1 import meanVolumes, L1centre, indicesTEndBack, indicesTEndForward, indicesTBegin, indicesL1Init
allMeanVolumes.append(meanVolumes), allL1centre.append(L1centre), allIndicesTEndBack.append(indicesTEndBack), allIndicesTEndForward.append(indicesTEndForward)
allIndicesTBegin.append(indicesTBegin), allIndicesL1Init.append(indicesL1Init)


allPlants.append("plant 2")
from config_2015_02_16_plant2 import rootPath, savePath, timesList, ALTDataPathList, segDataPathList
allRootPaths.append(rootPath), allSavePaths.append(savePath), allTimesList.append(timesList), allALTDataPathList.append(ALTDataPathList), allSegDataPathList.append(segDataPathList)
from indices_parameters_2015_02_16_plant2 import meanVolumes, L1centre, indicesTEndBack, indicesTEndForward, indicesTBegin, indicesL1Init
allMeanVolumes.append(meanVolumes), allL1centre.append(L1centre), allIndicesTEndBack.append(indicesTEndBack), allIndicesTEndForward.append(indicesTEndForward)
allIndicesTBegin.append(indicesTBegin), allIndicesL1Init.append(indicesL1Init)


allPlants.append("plant 4")
from config_2015_02_16_plant4 import rootPath, savePath, timesList, ALTDataPathList, segDataPathList
allRootPaths.append(rootPath), allSavePaths.append(savePath), allTimesList.append(timesList), allALTDataPathList.append(ALTDataPathList), allSegDataPathList.append(segDataPathList)
from indices_parameters_2015_02_16_plant4 import meanVolumes, L1centre, indicesTEndBack, indicesTEndForward, indicesTBegin, indicesL1Init
allMeanVolumes.append(meanVolumes), allL1centre.append(L1centre), allIndicesTEndBack.append(indicesTEndBack), allIndicesTEndForward.append(indicesTEndForward)
allIndicesTBegin.append(indicesTBegin), allIndicesL1Init.append(indicesL1Init)


allPlants.append("plant 13")
from config_2015_02_16_plant13 import rootPath, savePath, timesList, ALTDataPathList, segDataPathList
allRootPaths.append(rootPath), allSavePaths.append(savePath), allTimesList.append(timesList), allALTDataPathList.append(ALTDataPathList), allSegDataPathList.append(segDataPathList)
from indices_parameters_2015_02_16_plant13 import meanVolumes, L1centre, indicesTEndBack, indicesTEndForward, indicesTBegin, indicesL1Init
allMeanVolumes.append(meanVolumes), allL1centre.append(L1centre), allIndicesTEndBack.append(indicesTEndBack), allIndicesTEndForward.append(indicesTEndForward)
allIndicesTBegin.append(indicesTBegin), allIndicesL1Init.append(indicesL1Init)


allPlants.append("plant 15")
from config_2015_02_16_plant15 import rootPath, savePath, timesList, ALTDataPathList, segDataPathList
allRootPaths.append(rootPath), allSavePaths.append(savePath), allTimesList.append(timesList), allALTDataPathList.append(ALTDataPathList), allSegDataPathList.append(segDataPathList)
from indices_parameters_2015_02_16_plant15 import meanVolumes, L1centre, indicesTEndBack, indicesTEndForward, indicesTBegin, indicesL1Init
allMeanVolumes.append(meanVolumes), allL1centre.append(L1centre), allIndicesTEndBack.append(indicesTEndBack), allIndicesTEndForward.append(indicesTEndForward)
allIndicesTBegin.append(indicesTBegin), allIndicesL1Init.append(indicesL1Init)


allPlants.append("plant 18")
from config_2015_02_16_plant18 import rootPath, savePath, timesList, ALTDataPathList, segDataPathList
allRootPaths.append(rootPath), allSavePaths.append(savePath), allTimesList.append(timesList), allALTDataPathList.append(ALTDataPathList), allSegDataPathList.append(segDataPathList)
from indices_parameters_2015_02_16_plant18 import meanVolumes, L1centre, indicesTEndBack, indicesTEndForward, indicesTBegin, indicesL1Init
allMeanVolumes.append(meanVolumes), allL1centre.append(L1centre), allIndicesTEndBack.append(indicesTEndBack), allIndicesTEndForward.append(indicesTEndForward)
allIndicesTBegin.append(indicesTBegin), allIndicesL1Init.append(indicesL1Init)

